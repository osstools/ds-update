# Ds install and updates #

This only for hosting the install script and the updates for the _ds_ application.

### How do I install _ds_? ###

Run the below command in _bash_ and follow the prompts:

```
curl -LSs https://bitbucket.org/osstools/ds-update/downloads/ds_installer -o /tmp/ds_installer && chmod u+x /tmp/ds_installer && /tmp/ds_installer install
 ```

When installed, go to the sync root directory you entered (e.g. ~/Programming) and create or open a directory, then run `ds init` and follow the instructions.

Once this has finished, enter any Syncthing device ids (should be emailed to you on install or get it by running `ds sync:device:id`) by running `ds sync:device:add`.

You will need to do this once each way for each device.

### Who do I talk to? ###

* ashley@opensauce.systems